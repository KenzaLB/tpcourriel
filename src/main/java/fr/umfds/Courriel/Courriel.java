package fr.umfds.Courriel;

import java.util.ArrayList;



public class Courriel {
	
	//attributs 
	private String adresseDestination;
	private String titreMessage ;
	private String corpsMessage;
	private ArrayList<String> piecesJointes;
	
	//...........................Methode ENVOYER...............................................
	
	//LEVER UNE EXCEPTION ENVOIIMPOSSIBLE 
	
		public void envoyer() throws EnvoiImpossible
		{
			//verifier la presence d'une adresse mail de destination 
			if(!this.adressePresente())
			{
				throw new EnvoiImpossible("Pas de mail de destination ");
			}
			//verifier que l'adresse est  bien formée 
			if(!this.adresseBienFormee())
			{
				throw new EnvoiImpossible("Adresse mail de destination mal formée ");
			}
			//verifier presence de titre 
			if(!this.titrePresent())
			{
				throw new EnvoiImpossible("Titre du mail absent donc Envoi Impossible ");
			}
			//verifier la presence de pieces jointes si elle ont été mentionné dans le texte 
			if(this.MentionMotJoint() && !presencePJ())
			{
				throw new EnvoiImpossible("Ce courriel est supposé contenir des pieces jointes ");
			}
			
			
		}

	//constructeur par defaut 
	public Courriel()
	{
		this.corpsMessage="";
		this.adresseDestination="";
		this.titreMessage="";
		this.piecesJointes=new ArrayList<String>();
	}
	
	public Courriel(String adresse , String titre , String corps )
	{
		this.setAdresseDestination(adresse);
		this.setTitreMessage(titre);
		this.setCorpsMessage(corps);
		this.piecesJointes = new ArrayList<String>();
	}
	
	public Courriel(String ad ,String title ,String c , ArrayList<String> joints)
	{
		this.setAdresseDestination(ad);
		this.setCorpsMessage(c);
		this.setTitreMessage(title);
		this.piecesJointes=joints;
	}

	public void addPieceJointe(String chemin )
	{
		this.getPiecesJointes().add(chemin);
	}
	

	//accesseurs en lecture et ecriture 
	
	public String getTitreMessage() {
		return titreMessage;
	}

	public void setTitreMessage(String titreMessage) {
		this.titreMessage = titreMessage;
	}

	public String getCorpsMessage() {
		return corpsMessage;
	}

	public void setCorpsMessage(String corpsMessage) {
		this.corpsMessage = corpsMessage;
	}

	public ArrayList<String> getPiecesJointes() {
		return piecesJointes;
	}

	public String getAdresseDestination() {
		return adresseDestination;
	}

	public void setAdresseDestination(String adresseDestination) {
		this.adresseDestination = adresseDestination;
	}
	
	public void setPiecesJ(ArrayList<String> joint)
	{
		this.piecesJointes=joint;
	}
	
	
	public boolean presencePJ() {
		return !(this.getPiecesJointes().size()==0);
	}

	public boolean MentionMotJoint() {
		if (this.getCorpsMessage().contains("PJ") || (this.getCorpsMessage().contains("joint")) ||(this.getCorpsMessage().contains("jointe")))
		{
			return true;
		}else {
			return false;
		}
	}

	public boolean titrePresent() {
		
		return !(this.getTitreMessage().length()==0);
	}

	public boolean adresseBienFormee()
	{
		return this.getAdresseDestination().matches("^[aA-zZ]([aA-zZ0-9])*@([aA-zZ])*\\.([aA-zZ])*");
				
	}

	private boolean adressePresente() {
		return !(this.getAdresseDestination().length()==0);
	}

	
}
