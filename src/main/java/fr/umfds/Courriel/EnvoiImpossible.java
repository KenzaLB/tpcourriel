package fr.umfds.Courriel;

public class EnvoiImpossible extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EnvoiImpossible(String string)
	{
		super(string);
	}
}
