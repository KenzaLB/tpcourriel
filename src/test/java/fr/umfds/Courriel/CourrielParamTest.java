package fr.umfds.Courriel;

//import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
//import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
//import org.junit.jupiter.params.provider.CsvFileSource;


//import fr.umfds.Courriel.CourrielTestParam.AdresseMalForme.Adresses;



class CourrielTestParam {
	

	
	private Courriel c;

	
	@BeforeEach
	public void init()
	{
		c=new Courriel();
		c.setTitreMessage("agl");
		
	}
	/*test parametré prenant en parametre une chaine de carcatere address n'etant pas une adresse bien formée 
	et qui s'assure que address est bien detectée comme une adresse mal formée */
	
	
	/*
	 * 
	 * TEST UNITAIRE CLASSIQUE 
	 
	 	@Test
		public void testAdresseMalForme2()
    	{
    		//arrange 
			c=new Courriel();
    	
			//act 
    		c.setAdresseDestination("myriam102@gmail12.fr");
    		c.setTitreMessage("hello");
    		c.setCorpsMessage("hellocorps");
    		//assert 
    		EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
	    			c.envoyer();
	    			});
    		assertTrue(thrown.getMessage().equals("Adresse mail de destination mal formée "));
    		
    }
  
	  */
	
	
	
	
	//......................................@ValueSource..............................................
	
	@ParameterizedTest(name="{index} - run test with args = {0}")
	@ValueSource(strings= {"adresse1","adresse@123.fr","2addre.gmail.com","899"})
	public void testAdressValueSource(String adresse)
	{
		c.setAdresseDestination(adresse);
		EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
    		c.envoyer();
    		});
    	assertTrue(thrown.getMessage().equals("Adresse mail de destination mal formée "));
	}
	 	
		//............................... @MethodSource............................................... 
		private static Object[] badAddresses()
		{
			return new Object[][]{
				{"adresse","Pas d'arobase ni de point "},
				{"@eee.fr","adresse ne commençant pas par une lettre "},
				{"aaa@eee" ,"pas de point "},
			};
		}
	
	@ParameterizedTest(name="run with adress : {0} , problem : {1}")
	@MethodSource("badAddresses")
	public void testMethodeSourceAdresses(String adress ,String problem)
	{
		c.setAdresseDestination(adress);
		EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
    		c.envoyer();
    		});
    	assertTrue(thrown.getMessage().equals("Adresse mail de destination mal formée "));
    } 

	//.........................@CsvSource..........................................
	@ParameterizedTest(name="{index} - run with arg={0} and problem={1}")
	@CsvSource({
		"adresse ,Pas d'arobase ni de point ",
		"@eee.fr,adresse ne commençant pas par une lettre ",
		"aaa@eee ,pas de point "
		
	})
	public void csvSourceTest(String address ,String problem)
	{
		c.setAdresseDestination(address);
		EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
    		c.envoyer();
    		});
    	assertTrue(thrown.getMessage().equals("Adresse mail de destination mal formée "));
	}
	
	//.........................@CsvFileSource..........................................
	/*
	@ParameterizedTest
	@CsvFileSource(resources="testCSV.csv")
	public void csvFileTest(String address ,String problem)
	{
		c.setAdresseDestination(address);
		EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
    		c.envoyer();
    		});
    	assertTrue(thrown.getMessage().equals("Adresse mail de destination mal formée "));
    	
	}
	*/
	//...............................@EnumSource................................................
		/*
		//On n'arrive pas à faire une enumeration simple car les elements de l'enumeration contiennent des caracteres speciaux 
		
		public static enum Adresses{
		
				ADD1("adresse1"),
				ADD2("kenza102fr"),
				ADD3("899hello.fr"),
				ADD4("2addre.gmail.com");
			
			private final String getAdresse;
			
			//constructeur 
			//ca cree l'instance ADD1 Avec getAdresse="adresse1"
			Adresses(String string) {
				this.getAdresse=string;
			}

			@Override
			public String toString()
			{
				return this.getAdresse;
			}
			
		}
		
		
		
		@ParameterizedTest(name="{index} - run with args={0}")
		@EnumSource(Adresses.class)
		public void enumSourceTest(Adresses adresse)
		{
			c.setAdresseDestination(adresse.toString());
			EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
	    		c.envoyer();
	    		});
	    	assertTrue(thrown.getMessage().equals("Adresse mail de destination mal formée "));
		}
		
		*/

	

	


	
}
