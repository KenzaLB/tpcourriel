package fr.umfds.Courriel;

import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;




public class CourrielTest
{
	
	private Courriel c;
	private Courriel c2;
	
	
	
	//test de l'adresse mal formée 1
	@org.junit.jupiter.api.Test
	public void testAdresseMalForme1()
    {
    	//arrange 
		c=new Courriel();
    	
		//act 
    	c.setAdresseDestination("kenza102@kenza12.fr");
    	c.setTitreMessage("hello");
    	c.setCorpsMessage("hellocorps");
    	//assert 
    	EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
	    		c.envoyer();
	    		});
    	assertEquals(thrown.getMessage(),"Adresse mail de destination mal formée ");
    		
    }
	
	
	@Test
	public void testAdresseMalForme2()
    {
    	//arrange 
		c=new Courriel();
    	
		//act 
    	c.setAdresseDestination("myriam102@gmail12.fr");
    	c.setTitreMessage("hello");
    	c.setCorpsMessage("hellocorps");
    	//assert 
    	EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
	    		c.envoyer();
	    		});
    	assertTrue(thrown.getMessage().equals("Adresse mail de destination mal formée "));
    		
    }
	
	
  
	//test du constructeur parametré sans pieces jointes 
	@Test
	
	public void ConstructeurParametre()
	{
		//arrange
		c=new Courriel("myriam01@gmail.com","agl","rgtrfghj");
		//assert
		assertTrue(c.getAdresseDestination().equals("myriam01@gmail.com"));
		assertTrue(c.getTitreMessage().equals("agl"));
		assertTrue(c.getCorpsMessage().equals("rgtrfghj"));
		assertTrue(c.getPiecesJointes().size() == 0);
		
	}
	
	//test du constructeur parametré avec une arrayList de pieces jointes
	@Test
	public void ConstructeurParametrePJ()
	{
		//arrange 
		ArrayList<String> joints=new ArrayList<String>();
		//act
		joints.add("tp1");
		joints.add("tp2");
		c=new Courriel("kenza07.gmail.com","agl","fghjklmfPJ",joints);
		//assert
		assertTrue(c.getAdresseDestination().equals("kenza07.gmail.com"));
		assertTrue(c.getTitreMessage().equals("agl"));
		assertTrue(c.getCorpsMessage().equals("fghjklmfPJ"));
		assertTrue(c.getPiecesJointes().size() == 2);
	}
	
	
	
	//test de addPieceJointe(String chemin)
	@Test 
	public void testAddPJ()
	{
		//arrange 
				ArrayList<String> joints=new ArrayList<String>();
				//act
				joints.add("tp1");
				joints.add("tp2");
				c=new Courriel("kenza07.gmail.com","agl","fghjklmfPJ",joints);
				c.addPieceJointe("tp3");
				//assert
				assertTrue(c.getPiecesJointes().size()==3);
		
	}
	
	
	//test titre vide
	@Test
	public void testTitreVide()
	{
		//arrange
		c=new Courriel();
		//act 
    	c.setAdresseDestination("kenza102@kenza.fr");
    	c.setCorpsMessage("hellocorps");
    	//assert 
    	EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
    		c.envoyer();
    		});
    	assertTrue(thrown.getMessage().equals("Titre du mail absent donc Envoi Impossible "));
	}

	//test adresse vide 
	@Test
	public void testAdresseVide()
	{
		//arrange 
		c2 = new Courriel();
		
		//act 
		c2.setCorpsMessage("hello");
		c2.setTitreMessage("hello");
		//assert
		EnvoiImpossible thrown= assertThrows(EnvoiImpossible.class , ()-> {
    		c2.envoyer();
    		});
    	assertTrue(thrown.getMessage().equals("Pas de mail de destination "));
	}
	
	//test de mention de piece jointe sans piece jointe
	
	//test avec tout bon 
	@Test 
	public void testBon()
	{
		//arrange
		c=new Courriel();
		//act 
		c.setAdresseDestination("kenza@gmail.com");
		c.setTitreMessage("titre convenable");
		c.setCorpsMessage("message normal ");
		//assert
		try {
			c.envoyer();
		}catch(Exception e) {
			fail(e);
		}
	}
	
	
	@Test 
	public void testPJ()
	{
		//arrange
		c=new Courriel();
		
		//act 
		c.setAdresseDestination("kenza@gmail.com");
		c.setTitreMessage("hello");
		c.setCorpsMessage("ce message contient des PJ");
		EnvoiImpossible thrown =assertThrows(EnvoiImpossible.class , ()->{
			c.envoyer();
		});
		//assert
		assertTrue(thrown.getMessage().equals("Ce courriel est supposé contenir des pieces jointes "));
		
				
	}
	
	@Test
	public void presencePJ()
	{
		//arrange
		c=new Courriel();
		ArrayList<String> joints=new ArrayList<>();
		
		
		//act 
		joints.add("tp1");
		joints.add("tp2");
		c.setAdresseDestination("kenza@gm.com");
		c.setCorpsMessage("hello");
		c.setTitreMessage("agl");
		c.setPiecesJ(joints);
		//assert
		assertTrue(c.getAdresseDestination().equals("kenza@gm.com"));
		assertTrue(c.getCorpsMessage().equals("hello"));
		assertTrue(c.getTitreMessage().equals("agl"));
		assertTrue(c.presencePJ()==true);
		
		
	}
	

	 
	@Test
	void testMentionJointbranche1()
	{
		//arrange 
		c=new Courriel();
		ArrayList<String> joints=new ArrayList<>();
		
		
		//act 
		joints.add("tp1");
		joints.add("tp2");
		c.setAdresseDestination("kenza@gm.com");
		c.setCorpsMessage("hello corps contient PJ");
		c.setTitreMessage("agl");
		c.setPiecesJ(joints);
		//assert
		assertTrue(c.MentionMotJoint());
		
	}
	@Test
	void testMentionJointbranche2()
	{
		//arrange 
		c=new Courriel();
		ArrayList<String> joints=new ArrayList<>();
		
		
		//act 
		joints.add("tp1");
		joints.add("tp2");
		c.setAdresseDestination("kenza@gm.com");
		c.setCorpsMessage("hello corps contient joint");
		c.setTitreMessage("agl");
		c.setPiecesJ(joints);
		//assert
		assertTrue(c.MentionMotJoint());
		
	}
	
	
	@Test
	void testMentionJointbranche3()
	{
		//arrange 
		c=new Courriel();
		ArrayList<String> joints=new ArrayList<>();
		
		
		//act 
		joints.add("tp1");
		joints.add("tp2");
		c.setAdresseDestination("kenza@gm.com");
		c.setCorpsMessage("hello corps contient jointe");
		c.setTitreMessage("agl");
		c.setPiecesJ(joints);
		//assert
		assertTrue(c.MentionMotJoint());
		
	}
	
	}
    

